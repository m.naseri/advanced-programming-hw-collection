## M.Mehdi Naseri 9423115 ##

import os
import math as m
import time
import subprocess


class GaussSolver:
    def __init__(self, _a, _b, _n):
        self.m_A = _a
        self.m_B = _b
        self.m_N = _n
        self.m_Result = 0

    def legendre(self, m_N, x):
        if m_N == 0:
            return 1
        elif m_N == 1:
            return x
        else:
            return ((2.0 * m_N - 1) / m_N) * x * self.legendre(m_N - 1, x) - ((1.0 * m_N - 1) / m_N) \
                   * self.legendre(m_N - 2, x)

    def dLegendre(self, m_N, x):
        return (1.0 * m_N / (x * x - 1)) * ((x * self.legendre(m_N, x)) - self.legendre(m_N - 1, x))

    def legendreZeroes(self, m_N, i):
        xnew1, xold1 = 0, 0
        xold1 = m.cos(m.pi * (i - 1 / 4.0) / (m_N + 1 / 2.0))
        iteration = 1
        while True:
            if iteration != 1:
                xold1 = xnew1
            xnew1 = xold1 - self.legendre(m_N, xold1) / self.dLegendre(m_N, xold1)
            iteration += 1
            if 1 + abs((xnew1 - xold1)) <= 1.:
                break
        return xnew1

    def weight(self, m_N, x):
        return 2 / ((1 - pow(x, 2)) * pow(self.dLegendre(m_N, x), 2))

    def execute(self):
        integral = 0.
        for i in range(1, self.m_N + 1):
            integral += aFunction(self.legendreZeroes(self.m_N, i)) * \
                        self.weight(self.m_N, self.legendreZeroes(self.m_N, i))
        self.m_Result = ((self.m_B - self.m_A) / 2.0) * integral

    def getResult(self):
        return self.m_Result


def aFunction(x):
    xN = 0.5 * x + 0.5
    return (xN ** 3 / (xN + 1)) * m.cos(xN ** 2)


a = 0.
b = 1.
for i in range(0, 23):
    aSolver = GaussSolver(a, b, i)
    print("{}: ".format(i))
    # t0 = process_time()
    t0 = time.time()
    subprocess.call([os.path.join(os.getcwd(), "IntegrateC++"), str(i)])
    # t1 = process_time()
    t1 = time.time()
    print("C++ took: {:.5} s".format(t1 - t0))

    # t0 = process_time()
    t0 = time.time()
    aSolver.execute()
    # t1 = process_time()
    t1 = time.time()
    print("Result of Python code (n = {}): {}".format(i, aSolver.getResult()))
    print("Python took: {:.5} s".format(t1 - t0))
    print("----------")
