/** M.Mehdi Naseri 9423115 **/

#include <iostream>

void stringReplace(std::basic_string<char>&, const std::basic_string<char>&, const std::basic_string<char>&);

int main() {
    std::basic_string<char> str;
    std::cout << "Original STR: ";
    std::cin >> str;
    stringReplace(str, "11111", "111110");
    str.append("111111");
    std::cout << "Bit-stuffed: " << str << std::endl;
    return 0;
}

void stringReplace(std::basic_string<char>& str, const std::basic_string<char>& oldStr, const std::basic_string<char>& newStr) {
    std::string::size_type pos = 0u;
    while((pos = str.find(oldStr, pos)) != std::string::npos) {
        str.replace(pos, oldStr.length(), newStr);
        pos += newStr.length();
    }
}