/** M.MeHdi Naseri 9423115 **/

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

int main() {
    std::vector<int> myVector;
    std::cout << "PushBack: " << std::endl;
    std::cout << "Size: " << myVector.size() << "; Capacity: " << myVector.capacity() << "; \n";
    for(int i{}; i < 1000; i++) {
        myVector.push_back(i);
        std::cout << "Size: " << myVector.size() << "; Capacity: " << myVector.capacity() << "; \n";
    }

//    std::cout << "std::remove: " << std::endl;
//    for(int i{999}; i >= 0; i--) {
//        std::remove(myVector.begin(), myVector.end(), i);
//        std::cout << "Size: " << myVector.size() << "; Capacity: " << myVector.capacity() << "; \n";
//    }

//    std::cout << "erase(): " << std::endl;
//    for(int i{999}; i >= 0; i--) {
//        myVector.erase(myVector.begin(), myVector.begin() + 1);
//        std::cout << "Size: " << myVector.size() << "; Capacity: " << myVector.capacity() << "; \n";
//    }

    return 0;
}