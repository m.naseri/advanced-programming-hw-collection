/** M.Mehdi Naseri 9423115 **/

#include <iostream>
#include <algorithm>
#include <numeric>
#include <random>

int numberGenerator();
void showVector(std::vector<int>&);

int main() {
    std::vector<int> myVector(100);

    std::generate(myVector.begin(), myVector.end(), numberGenerator);
    std::cout << "Original: ";showVector(myVector);

    std::shuffle(myVector.begin(), myVector.end(), std::mt19937(std::random_device()()));
    std::cout << "Shuffle: ";
    showVector(myVector);

    double mean{static_cast<double>(std::accumulate(myVector.begin(), myVector.end(), 0)) / myVector.size()};
    std::cout << "Average: " << mean << std::endl;

    std::sort(myVector.begin(), myVector.end(), [&mean](int& x, int& y){return fabs(x - mean) > fabs(y - mean);});
    std::cout << "Sort: ";
    showVector(myVector);
    return 0;
}

int numberGenerator() {
    static int num{-50};
    return num++;
}

void showVector(std::vector<int>& v) {
    for(int& x : v) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}