/** M.Mehdi Naseri 9423115 **/

#include <iostream>
#include <algorithm>
#include <array>
#include <numeric>

void showArray(std::array<int, 10000>& arr);

int main() {
    srand(static_cast<unsigned int>(time(nullptr)));
    std::array<int, 10000> arr{};
    std::generate(arr.begin(), arr.end(), [](){return rand() % 10;});
    double mean {static_cast<double>(std::accumulate(arr.begin(), arr.end(), 0)) / arr.size()};
    std::cout << "Average: " << mean << std::endl;
    // showArray(arr);
    int randNum = rand() % 10;
    std::cout << "Q1: Number " << randNum << ": " << std::count(arr.begin(), arr.begin() + 2500, randNum)
              << " times;" << std::endl;
    std::cout <<  "Q2: Number " << randNum << ": " << std::count(arr.begin() + 2500, arr.begin() + 5000, randNum)
              << " times;" << std::endl;
    std::cout <<  "Q3: Number " << randNum << ": " << std::count(arr.begin() + 5000, arr.begin() + 7500, randNum)
              << " times;" << std::endl;
    std::cout <<  "Q4: Number " << randNum << ": " << std::count(arr.begin() + 7500, arr.begin() + 10000, randNum)
              << " times;" << std::endl;
    return 0;
}

void showArray(std::array<int, 10000>& arr) {
    for(int& x : arr) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}