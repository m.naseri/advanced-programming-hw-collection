#include <iostream>

void stringReplace(std::basic_string<char>&, const std::basic_string<char>&, const std::basic_string<char>&);

int main() {
    std::basic_string<char> str;
    std::basic_string<char> inputStr{};
    std::cout << "Enter Original STR: ";
    std::cin >> inputStr;

    std::cout << "Enter Bit-stuffed: ";
    std::cin >> str;

    if(str.substr(str.size() - 6, str.size()) != "111111") {
        std::cout << "ERROR" << std::endl;
        return -1;
    }
    str = str.substr(0, str.size() - 6);
    stringReplace(str, "111110", "11111");
    std::cout << "Bit-unstuffed: " << str << std::endl;

    if(str == inputStr)
        std::cout << "It works fine." << std::endl;

    return 0;
}

void stringReplace(std::basic_string<char>& str, const std::basic_string<char>& oldStr, const std::basic_string<char>& newStr) {
    std::string::size_type pos = 0u;
    while((pos = str.find(oldStr, pos)) != std::string::npos) {
        str.replace(pos, oldStr.length(), newStr);
        pos += newStr.length();
    }
}