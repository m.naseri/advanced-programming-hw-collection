## M.MeHdi Naseri 9423115 ##

import random
import os
from statistics import variance

for iterator in range(7):
    arr = []
    for i in range(10):
        subArr = []
        for j in range(5):
            subArr.append(random.randint(0, 100))
        arr.append(subArr)

    f = open("{}.txt".format(iterator), "w+")
    for i in range(10):
        for j in range(5):
            f.write(str(arr[i][j]) + ", ")
listDir = os.listdir()
listDir.append("notThere.txt")

for file in listDir:
    if file[-3:] != "txt":
        continue
    if os.path.exists(file):
        with open(file, 'r') as f:
            s = f.readlines()
            s = s[0].split(", ")
            print("{}: ".format(file))
            for i in range(10 * 5):
                s[i] = int(s[i])
            s.pop()
            sum = 0
            for i in range(4, 50, 5):
                sum += s[i]

            print("Sum: {}".format(sum))
            print("Variance: {}".format(variance(s)))
            print("----------")
