/** M.MeHdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#ifndef Q3_VECTOR_H
#define Q3_VECTOR_H

class Vector {
public:
    Vector(); // Constructor
    Vector(Vector&); // CopyConstructor
    ~Vector(); // Destructor
    Vector& operator=(const Vector&);
    Vector operator+(const Vector&);
    void push_back(int);
    unsigned int size();
    int max();
    void pop_back();
    void display();
    void Subvec(unsigned int);

private:
    unsigned int N;
    int* arr;
};


#endif //Q3_VECTOR_H
