/** M.MeHdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#include <iostream>
#include "Vector.h"

Vector::Vector() {
//    std::cout << "Constructor" << std::endl;
    N = 0;
    arr = nullptr;
}

Vector::Vector(Vector& v) {
//    std::cout << "CopyConstructor" << std::endl;
    N = v.N;
    arr = new int[N];
    for(unsigned int i{0}; i < N; i++) {
        arr[i] = v.arr[i];
    }
}

Vector::~Vector() {
    delete[] arr;
}

Vector& Vector::operator=(const Vector& v) {
    if(this == &v)
        return *this;
//    std::cout << "Operator =" << std::endl;
    delete[] arr;
    N = v.N;
    arr = new int[N];
    for(unsigned int i{0}; i < N; i++) {
        arr[i] = v.arr[i];
    }
    return *this;
}

void Vector::push_back(int number) {
    int* arrPrime{new int[N+1]};
    for(unsigned int i{0}; i < N; i++)
        arrPrime[i] = arr[i];
    delete[] arr;
    arrPrime[N++] = number;
    arr = arrPrime;
}

Vector Vector::operator+(const Vector& v) {
    Vector vOut{};
    vOut.N = v.N + N;
    vOut.arr = new int[vOut.N];
    for(unsigned int i{0}; i < N; i++)
        vOut.arr[i] = arr[i];
    for(unsigned int i{N}; i < vOut.N; i++)
        vOut.arr[i] = v.arr[i-N];
    return vOut;
}

void Vector::pop_back() {
    N--;
}

void Vector::display() {
    std::cout << "Vector Display: ";
    for(unsigned int i{0}; i < N; i++)
        std::cout << arr[i] << " ";
    std::cout << std::endl;
}

unsigned int Vector::size() {
    return N;
}

int Vector::max() {
    int max{arr[0]};
    for(unsigned int i{1}; i < N; i++)
        if(arr[i] > max)
            max = arr[i];
    return max;
}

void Vector::Subvec(unsigned int i) {
    if(i >= N) {
        std::cout << "ERROR: There is not such element." << std::endl;
        return;
    }
    std::cout << "Vector[" << i << "] : " << arr[i] << std::endl;
}
