/** M.MeHdi Naseri 9423115 **/
#include <iostream>
#include "Vector.h"

int main() {
    Vector v{};
    Vector v1{};
    Vector v2{};
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.Subvec(1);
    v1 = v;
    v.pop_back();
    v2 = v1 + v;
    std::cout << "size: " << v2.size() << "\n";
    std::cout << "max_size: " << v2.max() << "\n";
    v.display();

    return 0;
}
