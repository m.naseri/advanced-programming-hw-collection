/** M.Mehdi Naseri 9423115 **/
#include <iostream>
#include "point.h"

int main() {
    std::cout << "---Print---" << std::endl;
    point p0{0, 0};
    p0.print();
    std::cout << std::endl;

    std::cout << "---Distance---" << std::endl;
    point p1{3, 4};
    std::cout << p0.distance(&p1) << std::endl;
    std::cout << std::endl;

    std::cout << "---Line---" << std::endl;
    point p2{1, 3};
    line* l0{p0.Line(&p2)};
    l0->print();
    std::cout << std::endl;

    std::cout << "---Operator+---" << std::endl;
    point p3{p1 + p2};
    p3.print();
    std::cout << std::endl;

    std::cout << "---isParallel---" << std::endl;
    point p4{5, 10};
    line* l1{p1.Line(&p4)};
    l1->print();
    if(l0->isParallel(l1))
        std::cout << "isParallel" << std::endl;
    else
        std::cout << "isNotParallel" << std::endl;
    std::cout << std::endl;

    std::cout << "---isPerpendicular---" << std::endl;
    point p5{3, -1};
    line* l2{p0.Line(&p5)};
    l2->print();
    if(l0->isPerpendicular(l2))
        std::cout << "isPerpendicular" << std::endl;
    else
        std::cout << "isNotPerpendicular" << std::endl;
    std::cout << std::endl;


    std::cout << "---intersection---" << std::endl;
    point* pointStar0{l2->intersection(l0)};
    pointStar0->print();
    std::cout << std::endl;

    std::cout << "---parallel---" << std::endl;
    line* l3{l0->parallel(new point(-1, 20))};
    l3->print();
    std::cout << std::endl;

    return 0;
}
