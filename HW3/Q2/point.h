/** M.Mehdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#ifndef Q2_POINT_H
#define Q2_POINT_H


#include "line.h"

class point  {
public:
    point(int x, int y);
    int getX();
    int getY();
    int distance(point*);
    line* Line(point*);
    point operator+(point&);
    void print();

private:
    int x;
    int y;
};


#endif //Q2_POINT_H
