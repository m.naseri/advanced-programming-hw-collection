/** M.Mehdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#include <iostream>
#include <cmath>
#include "line.h"
#include "point.h"

line::line(point* p0, point* p1) {
    if(p0->getX() == p1->getX())
        if(p0->getY() == p1->getY()){
            std::cout << "ERROR: Same start and end." << std::endl;
            return;
        }
    this->p0 = p0;
    this->p1 = p1;
    deltaX = static_cast<double>(p1->getX() - p0->getX());
    deltaY = static_cast<double>(p1->getY() - p0->getY());
}

line::line() = default;

bool line::isParallel(line* l) {
    if(this->deltaX == 0) {
        return l->deltaX == 0; // false if only this->deltaX is equal to zero
    }

    if(l->deltaX == 0)
        return false; // only l->deltaX is equal to zero

    return (this->deltaY / this->deltaX) == (l->deltaY / l->deltaX);

}

bool line::isPerpendicular(line* l) {
    if(deltaX == 0) {
        return l->deltaY == 0;
    }
    if(l->deltaX == 0) {
        return this->deltaY == 0;
    }

    return ((this->deltaY / this->deltaX) * (l->deltaY / l->deltaX) == -1); // m0 * m1 = -1

}

point* line::intersection(line* l) {
    if(isParallel(l)) {
        std::cout << "ERROR: These 2 lines are parallel." << std::endl;
        return nullptr;
    }

    if(this->deltaX == 0) { // only mThis is infinity
        double mL{l->deltaY/l->deltaX};
        double bL{-mL * l->p0->getX() + l->p0->getY()};
        return new point(this->p0->getX(), static_cast<int>(mL*this->p0->getX() + bL));
    }

    if(l->deltaX == 0) { // only mL is infinity
        double mThis{this->deltaY/this->deltaX};
        double bThis{-mThis * this->p0->getX() + this->p0->getY()};
        return new point(l->p0->getX(), static_cast<int>(mThis*l->p0->getX() + bThis));
    }

    double mL{l->deltaY/l->deltaX};
    double bL{-mL * l->p0->getX() + l->p0->getY()};
    double mThis{this->deltaY/this->deltaX};
    double bThis{-mThis * this->p0->getX() + this->p0->getY()};

    double xEQ{(bL-bThis)/(mThis-mL)};
    return new point(static_cast<int>(xEQ), static_cast<int>(mL*xEQ + bL));
}

line *line::parallel(point* p) {
    if(this->deltaX == 0) { // mThis is infinity
        return new line(p, new point(p->getX(), p->getY()+1));
    }

    return new line(p, new point(p->getX() + 1, static_cast<int>(p->getY()+ deltaY/deltaX)));
}

void line::print() {
    if(deltaX == 0) {
        std::cout << "Line: X = " << p0->getX() << std::endl;
        return;
    }
    double m{deltaY/deltaX};
    double b{-m * p0->getX() + p0->getY()};
    std::cout << "Line: Y = " << m << "X + " << b << std::endl;
}
