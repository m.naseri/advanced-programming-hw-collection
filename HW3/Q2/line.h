/** M.Mehdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#ifndef Q2_LINE_H
#define Q2_LINE_H

class point;

class line {
public:
    line();
    line(point*, point*);
    bool isParallel(line*);
    bool isPerpendicular(line*);
    point* intersection(line*);
    line* parallel(point*);
    void print();

private:
    point* p0;
    point* p1;
    double deltaX;
    double deltaY;
};


#endif //Q2_LINE_H
