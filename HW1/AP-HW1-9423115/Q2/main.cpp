/* M.Mehdi Naseri 9423115 */

#include<iostream>
#include<math.h>
#include<iomanip>

double f(double x);

int main()
{
	double eps{1}; //epsilon (error)
	double x0{.5}, x1{1}, x2{0}; //x0: x[n-1], x1: x[n], x2: x[n+1]
	unsigned int M{15}; //setw parameter
	int abajeSheitun{10};
	/* First Row */
	std::cout << std::setw(4) << "i" << std::setw(M) << "xOld" << std::setw(M) 
		<< "xNew" << std::setw(M) << "f(xNew)" << std::setw(M) << "eps\n";
	/* First time */
	std::cout << std::setw(4) << 0 << std::setw(M) << x0 << std::setw(M) 
		<< x1 << std::setw(M) << f(x1) << std::setw(M) << eps << std::endl;
	for(unsigned int i{1}; eps >= 1e-4; i++)
	{
		x2 = (x0*f(x1) - x1*f(x0)) / (f(x1) - f(x0));
		eps = fabs((x2 - x1) / x1);
		std::cout << std::setw(4) << i << std::setw(M) << x0 << std::setw(M)
			<< x2 << std::setw(M) << f(x2) << std::setw(M) << eps << std::endl;
		x0 = x1; //xOld for next time
		x1 = x2; // xNew for next time
	}
	return 0;
}

double f(double x)
{
	//return x*x*x + x*x + x + 1;
	return 1/tanh(x) - log(x); // coth(x) = 1/tanh(x)
}