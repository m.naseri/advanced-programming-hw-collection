/* M.Mehdi Naseri 9423115 */

#include<iostream>

int f(const int& l, const int& r, int* arr);

int main()
{
	int n{}; // array length
	std::cout << "n: ";
	std::cin >> n;
	int* arr{new int[n]}; // dynamic array
	int sum{}; // ans
	for(int i{}; i < n; i++)
		std::cin >> arr[i]; // Initialize array members
	for(int l{}; l < n; l++)
		for(int r{l}; r < n; r++)
			sum += f(l, r, arr);
	std::cout << "ANSWER: " << sum << std::endl;
	delete[] arr;
	return 0;
}

int f(const int& l, const int& r, int* arr)
{
	int sum{};
	for(int i{l}; i <= r; i++)
		sum += arr[i];
	return sum;
}

