//
// Created by divar on 5/7/18.
//

#include "ctext.h"

CText::CText(std::string s) : text{s} {}

const std::string &CText::getText() const {
    return text;
}

void CText::setText(const std::string &str) {
    CText::text = str;
}
