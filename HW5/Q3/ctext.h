//
// Created by divar on 5/7/18.
//

#ifndef Q3_CTEX_H
#define Q3_CTEX_H


#include <string>

class CText {
public:
    CText(std::string);

    const std::string &getText() const;

    void setText(const std::string &str);

private:
    std::string text;
};


#endif //Q3_CTEX_H
