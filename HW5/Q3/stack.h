//
// Created by divar on 5/7/18.
//

#ifndef Q3_STACK_H
#define Q3_STACK_H

template <class T>
class Stack {
    class Item;

    class Item {
    public:
        T content;
        Item* pNext;
        Item(T cn, Item* pN) : content{cn}, pNext{pN}{};
    };
    Item* pTop{};
    int count{0};
public:
    Stack() = default;
    Stack(const Stack&) = delete;
    Stack& operator=(const Stack&) = delete;
    ~Stack();
    void push(T);
    int getCount();
    bool isEmpty();
    T pop();
};

#include "stack.hpp"
#endif //Q3_STACK_H
