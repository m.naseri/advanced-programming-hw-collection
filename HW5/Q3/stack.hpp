//
// Created by divar on 5/7/18.
//

#include "stack.h"

template<class T>
Stack<T>::~Stack() {
    Item* pTemp{};
    while(pTop) {
        pTemp = pTop;
        pTop = pTop->pNext;
        delete pTemp;
    }
}

template<class T>
void Stack<T>::push(T content) {
    pTop = new Item(content, pTop);
    count += 1;
}

template<class T>
int Stack<T>::getCount() {
    return count;
}

template<class T>
bool Stack<T>::isEmpty() {
    return !count;
}

template<class T>
T Stack<T>::pop() {
    if(!pTop) {
        std::cout << "ERROR: Stack is empty.\n";
        return T();
    }
    T tTemp = pTop->content;
    Item* pTemp = pTop;
    pTop = pTop -> pNext;
    count -= 1;
    delete pTemp;
    return tTemp;
}
