//
// Created by divar on 5/6/18.
//

#include "queue.h"

template<class T>
void Queue<T>::push_back(T content) {
    pBack->pNext = new Item(content, nullptr);
    pBack = pBack->pNext;
}

template<class T>
void Queue<T>::display() {
    Item* pTemp{pFront->pNext};
    unsigned int counter{};
    while(pTemp != nullptr) {
        std::cout << pTemp->pContent << ", ";
        pTemp = pTemp->pNext;
        counter++;
    }
    std::cout << "\b\b";
    std::cout << " -> " << counter << " item\n";
}

template<class T>
void Queue<T>::remove(unsigned int index) {
    Item* pTemp{pFront};
    for(unsigned int i{}; i < index; i++) {
        if(pTemp->pNext == nullptr) {
            std::cout << "ERROR: out of queue." << std::endl;
            return;
        }
        pTemp = pTemp->pNext;
    }
    if(pTemp == pBack) {
        std::cout << "ERROR: out of queue." << std::endl;
        return;
    }
    Item* pTempPrime{pTemp->pNext};

    if(pTemp->pNext == pBack) {
        pBack = pTemp;
    }

    pTemp->pNext = pTemp->pNext->pNext;
    delete[] pTempPrime;
}

template<class T>
bool Queue<T>::empty() {
    return (pFront == pBack);
}

template<class T>
T Queue<T>::pop_front() {
    if(pFront == pBack) {
        std::cout << "ERROR: queue is empty.\n";
        return T();
    }
    Item* pTemp{pFront->pNext};
    if(pTemp == pBack) { // LastPOP
        pBack = pFront;
    }
    T tTemp{pTemp->pContent};
    pFront->pNext = pTemp->pNext;
    delete pTemp;
    return tTemp;
}

template<class T>
void Queue<T>::insert(unsigned int index, T content) {
    Item* pTemp{pFront};
    for(unsigned int i{}; i < index; i++) {
        if(pTemp->pNext == nullptr) {
            std::cout << "ERROR: out of queue." << std::endl;
            return;
        }
        pTemp = pTemp->pNext;
    }

    pTemp->pNext = new Item(content, pTemp->pNext);

    if(pTemp == pBack) {
        pBack = pTemp->pNext;
    }
}

template<class T>
Queue<T>::~Queue() {
    while(pFront != nullptr) {
        Item* pTemp{pFront};
        pFront = pFront->pNext;
        delete pTemp;
    }
}


