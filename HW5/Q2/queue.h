//
// Created by divar on 5/6/18.
//

#ifndef Q2_QUEUE_H
#define Q2_QUEUE_H

template <class T>
class Queue {
private:
    class Item;
    class Item {
    public:
        Item* pNext;
        T pContent;
        Item(T pC, Item* pN) : pContent{pC}, pNext{pN} {};
    };
    Item* pFront{new Item(T(), nullptr)};
    Item* pBack{pFront};
public:
    Queue() = default;
    ~Queue();
    Queue(Queue&) = delete;
    Queue& operator=(const Queue&) = delete;
    void push_back(T);
    void display();
    void insert(unsigned int, T);
    void remove(unsigned int);
    bool empty();
    T pop_front();
};

#include "queue.hpp"
#endif //Q2_QUEUE_H
