//
// Created by divar on 5/6/18.
//

#include <iostream>
#include <utility>
#include "Test.h"

Test::Test(std::string s) {
    str = std::move(s);
}

void Test::display() {
    std::cout << str << " DISPLAY\n";
}

void Test::setStr(const std::string &str) {
    Test::str = str;
}

const std::string &Test::getStr() const {
    return str;
}
