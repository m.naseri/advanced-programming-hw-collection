//
// Created by divar on 5/6/18.
//

#include "shared_ptr.h"

template<class T>
shared_ptr<T>::shared_ptr(T* p) {
    std::cout << "Constructor " << p->getStr() << std::endl;
    ptr = p;
    counterPtr = new unsigned int{1};
}

template<class T>
shared_ptr<T>::~shared_ptr() {
    (*counterPtr)--;
    if(!(*counterPtr)) {
        std::cout << "Destructor " << ptr->getStr() << std::endl;
        delete counterPtr;
        delete ptr;
    }
}

template<class T>
shared_ptr<T>::shared_ptr(shared_ptr & shPtr) {
    ptr = shPtr.ptr;
    counterPtr = shPtr.counterPtr;
    (*counterPtr)++;
}

template<class T>
unsigned int shared_ptr<T>::use_count() {
    return *counterPtr;
}

template<class T>
T *shared_ptr<T>::operator->() {
    return ptr;
}
