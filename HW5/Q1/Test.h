//
// Created by divar on 5/6/18.
//

#ifndef Q1_TEST_H
#define Q1_TEST_H


#include <string>

class Test {
public:
    explicit Test(std::string);
    ~Test() = default;
    void display();

    void setStr(const std::string &str);

    const std::string &getStr() const;

private:
    std::string str;
};


#endif //Q1_TEST_H
