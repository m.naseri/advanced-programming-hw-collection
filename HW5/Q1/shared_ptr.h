//
// Created by divar on 5/6/18.
//
//
#ifndef Q1_SHARED_PTR_H
#define Q1_SHARED_PTR_H

template <class T>
class shared_ptr {
public:
    explicit shared_ptr(T*);
    ~shared_ptr();
    shared_ptr(shared_ptr&);
    unsigned int use_count();
    T* operator->();
private:
    unsigned int* counterPtr;
    T* ptr;
};

#include "shared_ptr.hpp"
#endif //Q1_SHARED_PTR_H
