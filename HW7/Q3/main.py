# M.Mehdi Naseri 9423115 #

import matplotlib

matplotlib.use("QT5Agg")
import sys
import os
import numpy as np
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QFileDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from openpyxl import load_workbook, Workbook

Form = uic.loadUiType(os.path.join(os.getcwd(), 'gui.ui'))[0]


class IntroWindow(Form, QMainWindow):
    def __init__(self):
        Form.__init__(self)
        QMainWindow.__init__(self)
        self.setupUi(self)

        self.fig = Figure()
        self.ax = self.fig.add_axes([0.1, 0.1, 0.9, 0.9], frameon=False)
        self.x = []
        self.y = []
        self.scatter = self.ax.scatter(self.x, self.y)
        self.fileScatter = [self.ax.scatter([], []) for i in range(2)]
        self.ax.grid()
        self.canvas = FigureCanvas(self.fig)
        l = QVBoxLayout(self.matplotlibWidget)
        l.addWidget(self.canvas)

        self.pathOfFiles = [None, None]
        self.addPointButton.clicked.connect(self.addPoint)
        self.clearPointButton.clicked.connect(self.clearPoint)
        self.addFileButton.clicked.connect(self.addFile)
        self.drawFileButton.clicked.connect(self.drawFile)

    def addFile(self):
        filePath = QFileDialog.getOpenFileName(self, 'Open file', os.getcwd(), "*.xlsx")[0]
        if self.pathOfFiles[0] is None:
            self.pathOfFiles[0] = filePath
        else:
            self.pathOfFiles[1] = filePath

    def drawFile(self):
        if self.fileScatter[0]:
            self.fileScatter[0].remove()
        if self.fileScatter[1]:
            self.fileScatter[1].remove()
        for j in range(len(self.pathOfFiles)):
            if self.pathOfFiles[j] is None:
                self.fileScatter[j] = self.ax.scatter([], [])
                continue
            innerWb = load_workbook(str(self.pathOfFiles[j]))
            sheet_ranges = innerWb[innerWb.sheetnames[0]]
            xArr = []
            yArr = []
            i = 1
            while True:
                xP = sheet_ranges['A' + str(i)].value
                if sheet_ranges['A' + str(i)].value is None:
                    break
                yP = sheet_ranges['B' + str(i)].value
                xArr.append(xP)
                yArr.append(yP)
                i += 1
            self.fileScatter[j] = self.ax.scatter(xArr, yArr)
        self.fig.canvas.draw()

    def addPoint(self):
        xC = self.xDoubleSpinBox.value()
        yC = self.yDoubleSpinBox.value()
        for i in range(len(self.x)):
            if self.x[i] == xC:
                if self.y[i] == yC:
                    return
        self.x.append(self.xDoubleSpinBox.value())
        self.y.append(self.yDoubleSpinBox.value())
        self.scatter.remove()
        self.scatter = self.ax.scatter(self.x, self.y)
        self.fig.canvas.draw()

    def clearPoint(self):
        xC = self.xDoubleSpinBox.value()
        yC = self.yDoubleSpinBox.value()
        for i in range(len(self.x)):
            if self.x[i] == xC and self.y[i] == yC:
                self.scatter.remove()
                self.x.pop(i)
                self.y.pop(i)
                self.scatter = self.ax.scatter(self.x, self.y)
                self.fig.canvas.draw()
                return


A = [np.random.randn() for i in range(100)]
B = np.linspace(-5, 5, 100)
wb = Workbook(write_only=True)
ws = wb.create_sheet()
for i in range(100):
    ws.append([A[i], B[i]])
wb.save('data.xlsx')

app = QApplication(sys.argv)
w = IntroWindow()
w.show()
sys.exit(app.exec())
