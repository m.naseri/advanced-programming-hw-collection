#include "mathfuncs.h"
#include <iostream>
#include <string>
using std::string;

mathfuncs::mathfuncs() {}

std::string mathfuncs::Fibonacci(std::string a) {
	string strOld{ "0" };
	string strNew{ "1" };
	if (a == "0")
		return "0";

	int aInt{ std::stoi(a) };
	for (int i{ 0 }; i < aInt - 1; i++) {
		string strTemp{ strNew };
		strNew = findSum(strNew, strOld);
		strOld = strTemp;
	}

	return strNew;
}

mathfuncs::~mathfuncs() {}

string mathfuncs::findSum(string& str1, string& str2) {
	if (str1.length() > str2.length())
		swap(str1, str2);
	string str = "";
	int n1 = str1.length(), n2 = str2.length();
	int diff = n2 - n1;
	int carry = 0;

	for (int i = n1 - 1; i >= 0; i--) {
		int sum = ((str1[i] - '0') +
			(str2[i + diff] - '0') +
			carry);
		str.push_back(sum % 10 + '0');
		carry = sum / 10;
	}

	for (int i = n2 - n1 - 1; i >= 0; i--) {
		int sum = ((str2[i] - '0') + carry);
		str.push_back(sum % 10 + '0');
		carry = sum / 10;
	}

	if (carry)
		str.push_back(carry + '0');
	reverse(str.begin(), str.end());

	return str;
}

char* Fibonacci(char* a)
{
	mathfuncs X{};
	std::string str{ X.Fibonacci(std::string(a)) };
	char *cstr = new char[str.length() + 1];

	strcpy_s(cstr, std::strlen(cstr), str.c_str());
	return cstr;
}

std::string CFibonacci(std::string a)
{
	mathfuncs X{};
	return X.Fibonacci(a);
}