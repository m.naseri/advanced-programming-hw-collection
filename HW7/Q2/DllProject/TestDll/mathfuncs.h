#pragma once
#include <string>

extern "C" {
	__declspec(dllexport) char* Fibonacci(char* a);
	__declspec(dllexport) std::string CFibonacci(std::string a);
}

class mathfuncs {
public:
	mathfuncs();
	~mathfuncs();
	std::string findSum(std::string& str1, std::string& str2);
	std::string Fibonacci(std::string a);
private:
};