/* M.Mehdi Naseri */
#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <string>

extern "C" {
	__declspec(dllimport) char* Fibonacci(char* a);
	__declspec(dllimport) std::string CFibonacci(std::string a);
}
int main() {
	std::ifstream inFile;
	std::ofstream outFile;
	outFile.open("Fibonacci_C++_runtime");
	inFile.open("Q2.txt");
	if (!inFile || !outFile) {
		std::cout << "Unable to open file";
		exit(1);
	}

	std::string x{};
	while (inFile >> x) {
		auto t1{ std::chrono::high_resolution_clock::now() };
		std::string y{ CFibonacci(x) };
		auto t2{ std::chrono::high_resolution_clock::now() };
		std::cout << x << "th: " << y << std::endl;
		outFile << x << " " << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() << "\n";
	} 

	inFile.close();
	outFile.close(); 
	getchar();
	return 0;
}