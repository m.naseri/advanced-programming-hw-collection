# M.Mehdi Naseri 9423115 #

import ctypes as ct
import os
import sys
import time
import matplotlib.pyplot as plt

dll_path = os.path.join(r"C:\Users\sHDiV4RHs\Desktop\Q2\Q2VS\DllProject\x64\Debug", "TestDLL.dll")
if not os.path.exists(dll_path):
    print("dll_path not found")
    sys.exit(1)

dll = ct.cdll.LoadLibrary(dll_path)
Fibonacci = dll.Fibonacci
Fibonacci.argtypes = [ct.c_char_p]
Fibonacci.restype = ct.c_char_p

with open(r"C:\Users\sHDiV4RHs\Desktop\Q2\Q2VS\DllProject\DllProject\Q2.txt", "r") as f:
    data = f.readlines()
    for number in data:
        number = number.split()[0]
        print(number + "th")
        t0 = time.time()
        print(Fibonacci(ct.c_char_p(number.encode('utf-8'))).decode("utf-8"))
        print("Took: {}ns".format(1e6*(time.time()- t0)))
        print("---------------------------")

with open(r"C:\Users\sHDiV4RHs\Desktop\Q2\Q2VS\DllProject\DllProject\Fibonacci_c++_runtime", "r") as f:
    x = []
    y = []
    data = f.readlines()
    for line in data:
        numbers = line.split()
        x.append(int(numbers[0]))
        y.append(int(numbers[1]))
    plt.scatter(x, y)
    plt.show()
