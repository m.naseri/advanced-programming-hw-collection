# M.Mehdi Naseri 9423115 #
import os
import zipfile
import re
import shutil

from openpyxl import Workbook


def unzipFile(filePath, targetPath):
    zip_ref = zipfile.ZipFile(filePath, 'r')
    if os.path.exists(targetPath):
        shutil.rmtree(targetPath)
    zip_ref.extractall(targetPath)
    zip_ref.close()


def getNecessaryFiles(filePath):
    finalList = []
    with open(filePath) as file:
        lines = file.readlines()
        for i in range(len(lines)):
            line = lines[i].split()
            if i == 0:
                finalList.append(line)
            else:
                toRemoveList = []
                for j in range(len(line)):
                    line[j] = re.sub("[^a-zA-z0-9]", "", line[j])
                    if line[j] == '':
                        toRemoveList.append(j)
                for j in toRemoveList:
                    line.pop(j)
                finalList.append(line)
    return finalList


def makeFolders(folderName, necessaryFiles):
    path = os.path.join(os.getcwd(), folderName)
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)

    for word in necessaryFiles:
        if len(word) > 1:
            os.makedirs(os.path.join(path, word[0]))


def isFormatRight(listDir, necessaryFiles):
    fileName = listDir[0]
    preFile = necessaryFiles[0][0]
    if fileName[0:len(preFile)] != preFile:
        return False
    if fileName[-3:] != 'zip':
        return False
    return True


def isFormatZip(listDir):
    fileName = listDir[0]
    if fileName[-3:] == 'zip':
        return True
    return False


def copyNecessaryFiles(path, studentNum, necessaryFiles):
    copyCompleted = False
    for i in range(1, len(necessaryFiles)):
        arr = necessaryFiles[i]
        if os.path.exists(os.path.join(path, arr[0])):
            for file in os.listdir(os.path.join(path, arr[0])):
                for j in range(1, len(necessaryFiles[i])):
                    if file.endswith(necessaryFiles[i][j]):
                        sourcePath = os.path.join(path, necessaryFiles[i][0], file)
                        destSource = os.path.join(os.getcwd(), 'Categorized_HW2',
                                                  necessaryFiles[i][0], studentNum + '_' + file)
                        shutil.copyfile(sourcePath, destSource)
                        copyCompleted = True
    return copyCompleted


def getStudentNum(zipFileName):
    return zipFileName[len('AP-HW2') + 1:-len('zip') - 1]


def writeNameOnExcelFile(badPeople):
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    for i in range(len(badPeople)):
        ws.append([badPeople[i]])
    wb.save('badPeople.xlsx')


unzipFile(os.path.join(os.getcwd(), "AP-HW2.zip"), os.path.join(os.getcwd(), "AP-HW2"))
necessaryFiles = getNecessaryFiles(os.path.join(os.getcwd(), "Q6.txt"))
makeFolders("Categorized_HW2", necessaryFiles)

badPeople = []

cwd = os.path.join(os.getcwd(), "AP-HW2")
for folder in os.listdir(cwd):
    listDir = os.listdir(os.path.join(cwd, folder))

    if isFormatRight(listDir, necessaryFiles):
        try:
            studentNum = getStudentNum(listDir[0])
            unzipFile(os.path.join(cwd, folder, listDir[0]), os.path.join(cwd, folder))
            listDir = os.listdir(os.path.join(cwd, folder))
            noParentDirectoryFlag = False
            directoryName = None

            for name in listDir:
                if os.path.isdir(os.path.join(cwd, folder, name)):
                    if name == 'Q2' or name == 'q2':
                        noParentDirectoryFlag = True
                if name[-3:] != 'zip' and name[-3:] != 'pdf':
                    directoryName = name
            path = os.path.join(os.getcwd(), 'AP-HW2', folder)

            if not noParentDirectoryFlag:
                path = os.path.join(path, directoryName)

            if not copyNecessaryFiles(path, studentNum, necessaryFiles):
                badPeople.append(re.sub('_.*$', "", folder))
                # print(re.sub('_.*$', "", folder))
        except:
            badPeople.append(re.sub('_.*$', "", folder))
    else:
        badPeople.append(re.sub('_.*$', "", folder))

if os.path.exists(os.path.join(os.getcwd(), "AP-HW2")):
    shutil.rmtree(os.path.join(os.getcwd(), "AP-HW2"))

writeNameOnExcelFile(badPeople)
