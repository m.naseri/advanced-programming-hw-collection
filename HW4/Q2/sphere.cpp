//
// Created by divar on 4/10/18.
//

#include "sphere.h"
#include <cmath>
#include <iostream>

Sphere::Sphere(double radius, double x, double y, double z) {
    if(radius < 0) {
        std::cout << "ERROR: radius can not be lower than zero.";
        return;
    }
    this->x = x;
    this->y = y;
    this->z = z;
    this->radius = radius;
}

double Sphere::area() {
    return 4 * acos(-1) * radius * radius;
}

double Sphere::volume() {
    return 4. / 3 * acos(-1) * radius * radius * radius;
}

std::ostream &Sphere::print(std::ostream &os) {
    os << "Sphere radius = " << radius << "\n"
       << "center --> (" << x << ", " << y << ", " << z << ")\n"
       << "area of " << area() << " & volume of " << volume() << "\n";
    return os;
}

Sphere Sphere::operator+(point& p) {
    Sphere sphere(radius, x + p.getX(), y + p.getY(), z);
    return sphere;
}
