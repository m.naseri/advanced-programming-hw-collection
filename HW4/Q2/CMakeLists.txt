cmake_minimum_required(VERSION 3.10)
project(Q2)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)

add_executable(Q2
        circle.cpp
        circle.h
        cube.cpp
        cube.h
        line.cpp
        line.h
        main.cpp
        point.cpp
        point.h
        shape.cpp
        shape.h
        sphere.cpp
        sphere.h
        square.cpp
        square.h
        threeDimensionalShape.cpp
        threeDimensionalShape.h
        twoDimensionalShape.cpp
        twoDimensionalShape.h)
