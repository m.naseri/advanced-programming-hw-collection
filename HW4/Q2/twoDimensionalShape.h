//
// Created by divar on 4/10/18.
//

#ifndef Q2_TWODIMENSIONALSHAPE_H
#define Q2_TWODIMENSIONALSHAPE_H


#include "shape.h"
#include <iostream>

class TwoDimensionalShape : public Shape {
public:
    TwoDimensionalShape() = default;
protected:
};


#endif //Q2_TWODIMENSIONALSHAPE_H
