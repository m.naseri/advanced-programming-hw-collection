//
// Created by divar on 4/10/18.
//

#ifndef Q2_CIRCLE_H
#define Q2_CIRCLE_H

#include <iostream>
#include "twoDimensionalShape.h"

class Circle : public TwoDimensionalShape {
public:
    explicit Circle(double radius, double x = 0, double y = 0);
    Circle() = default;
    double area() override;
    std::ostream& print(std::ostream& os) override;
    Circle operator+(point&);
protected:
    double radius{};
};

#endif //Q2_CIRCLE_H
