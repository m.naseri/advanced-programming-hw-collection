//
// Created by divar on 4/10/18.
//

#ifndef Q2_CUBE_H
#define Q2_CUBE_H


#include "threeDimensionalShape.h"

class Cube : public ThreeDimensionalShape{
public:
    explicit Cube(double length, double x = 0, double y = 0, double z = 0);
    double area();
    double volume();
    std::ostream& print(std::ostream& os);
    Cube operator+(point&);
private:
    double length;
};


#endif //Q2_CUBE_H
