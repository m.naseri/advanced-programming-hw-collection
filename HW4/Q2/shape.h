//
// Created by divar on 4/10/18.
//

#ifndef Q2_SHAPE_H
#define Q2_SHAPE_H
#include <iostream>
#include "point.h"

class Shape {
public:
    Shape() = default;
    virtual double area();
    virtual std::ostream& print(std::ostream& os);
    friend std::ostream &operator<<(std::ostream& os, Shape& m);
protected:
    double x{};
    double y{};
};


#endif //Q2_SHAPE_H
