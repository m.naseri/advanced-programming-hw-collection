//
// Created by divar on 4/10/18.
//

#ifndef Q2_THREEDIMENSIONALSHAPE_H
#define Q2_THREEDIMENSIONALSHAPE_H


#include "shape.h"
#include <iostream>

class ThreeDimensionalShape : public Shape {
public:
    ThreeDimensionalShape() = default;
    virtual double volume();
protected:
    double z{};
};


#endif //Q2_THREEDIMENSIONALSHAPE_H
