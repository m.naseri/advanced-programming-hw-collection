//
// Created by divar on 4/10/18.
//

#ifndef Q2_SQUARE_H
#define Q2_SQUARE_H


#include "twoDimensionalShape.h"

class Square : public TwoDimensionalShape {
public:
    explicit Square(double length, double x = 0, double y = 0);
    double area() override;
    std::ostream& print(std::ostream& os) override;
    Square operator+(point&);
protected:
    double length;
};


#endif //Q2_SQUARE_H
