//
// Created by divar on 4/10/18.
//

#include "circle.h"
#include <cmath>
#include <iostream>

Circle::Circle(double radius, double x, double y) {
    if(radius < 0) {
        std::cout << "ERROR: radius can not be lower than zero.";
        return;
    }
    this->x = x;
    this->y = y;
    this->radius = radius;
}

double Circle::area() {
    return acos(-1) * radius * radius;
}

std::ostream& Circle::print(std::ostream& os) {
    os << "Circle radius = " << radius << "\n"
              << "center --> (" << x << ", " << y <<")\n"
              << "area of " << area() << "\n";
    return os;
}

Circle Circle::operator+(point& p) {
    Circle circle(radius, x + p.getX(), y + p.getY());
    return circle;
}

