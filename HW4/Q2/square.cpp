//
// Created by divar on 4/10/18.
//

#include <iostream>
#include "square.h"

Square::Square(double length, double x, double y) {
    if(length < 0) {
        std::cout << "ERROR: length can not be lower than zero.";
        return;
    }
    this->x = x;
    this->y = y;
    this->length = length;
}

double Square::area() {
    return length*length;
}

std::ostream &Square::print(std::ostream &os) {
    os << "Square side length = " << length << "\n"
              << "center --> (" << x << ", " << y <<")\n"
              << "area of " << area() << "\n";
    return os;
}

Square Square::operator+(point& p) {
    Square square(length, x + p.getX(), y + p.getY());
    return square;
}
