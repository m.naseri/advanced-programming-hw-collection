//
// Created by divar on 4/10/18.
//

#ifndef Q2_SPHERE_H
#define Q2_SPHERE_H


#include "threeDimensionalShape.h"

class Sphere : public ThreeDimensionalShape {
public:
    Sphere(double radius, double x = 0, double y = 0, double z = 0);
    double area() override;
    double volume() override;
    std::ostream& print(std::ostream& os) override;
    Sphere operator+(point&);
private:
    double radius;
};


#endif //Q2_SPHERE_H
