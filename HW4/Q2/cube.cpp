//
// Created by divar on 4/10/18.
//

#include <iostream>
#include "cube.h"

Cube::Cube(double length, double x, double y, double z) {
    if(length < 0) {
        std::cout << "ERROR: length can not be lower than zero.";
        return;
    }
    this->x = x;
    this->y = y;
    this->z = z;
    this->length = length;
}

double Cube::area() {
    return 6 * length * length;
}

double Cube::volume() {
    return length * length * length;
}

std::ostream& Cube::print(std::ostream& os) {
    os << "cube side length = " << length << "\n"
              << "center --> (" << x << ", " << y << ", " << z << ")\n"
              << "area of " << area() << " & volume of " << volume() << "\n";
    return os;
}

Cube Cube::operator+(point& p) {
    Cube cube(length, x + p.getX(), y + p.getY(), z);
    return cube;
}
