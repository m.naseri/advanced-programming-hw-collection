/** M.Mehdi Naseri 9423115 **/
//
// Created by divar on 3/17/18.
//

#include <iostream>
#include <cmath>
#include "point.h"

point::point(int x, int y) {
    this->x = x;
    this->y = y;
}

int point::getX() {
    return x;
}

int point::getY() {
    return y;
}

int point::distance(point* p) {
    return static_cast<int>(sqrt(pow(x - p->x, 2) + pow(y - p->y, 2)));
}

line* point::Line(point* p) {
    return new line(this, p);
}

point point::operator+(point& p) {
    return point(this->x + p.x, this->y + p.y);
}

void point::print() {
    std::cout << "Point: (" << x << ", " << y << ")" << std::endl;
}

void point::setX(int x) {
    this->x = x;
}

void point::setY(int y) {
    this->y = y;
}
