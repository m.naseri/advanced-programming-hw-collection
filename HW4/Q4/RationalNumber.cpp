/** M.Mehdi Naseri 9423115 **/

//
// Created by divar on 4/12/18.
//

#include <iostream>
#include "RationalNumber.h"

RationalNumber::RationalNumber(int p, unsigned int q) {
    if(q <= 0) {
       std::cout << "ERROR: denominator should be greater than zero." << std::endl;
       return;
    }
    a = p;
    b = q;
    simpleForm(a, b);
}

void RationalNumber::simpleForm(int &a, unsigned int &b) {
    if(a == 0) {
        a = 0;
        b = 1;
        return;
    }

    if(abs(a) == b)
    {
        b = 1;
        if(a < 0) {
            a = -1;
            return;
        }
        a = 1;
        return;
    }

    unsigned int greaterNumber{(abs(a) > b) ? abs(a) : b};
    unsigned int lowerNumber{(abs(a) < b) ? abs(a) : b};
    for(unsigned int i{lowerNumber}; i > 1; i--) {
        if(lowerNumber % i == 0)
            if(greaterNumber % i == 0) {
                lowerNumber /= i;
                greaterNumber /= i;
                break;
            }
    }

    if(abs(a) > b) {
        b = lowerNumber;
        if(a < 0) {
            a = -greaterNumber;
        }
        else {
            a = greaterNumber;
        }
    } else {
        b = greaterNumber;
        if(a < 0) {
            a = -lowerNumber;
        }
        else {
            a = lowerNumber;
        }
    }
}

void RationalNumber::show() {
    if(a == 0) {
        std::cout << 0;
        return;
    }
    if(b == 1) {
        std::cout << a;
        return;
    }

    std::cout << a <<"/"<< b;
}

RationalNumber RationalNumber::operator+(RationalNumber &rationalNumber) {
    return RationalNumber(a*rationalNumber.b + b*rationalNumber.a, b*rationalNumber.b);
}

RationalNumber RationalNumber::operator-(RationalNumber &rationalNumber) {
    return RationalNumber(a*rationalNumber.b - b*rationalNumber.a, b*rationalNumber.b);
}

RationalNumber RationalNumber::operator*(RationalNumber &rationalNumber) {
    return RationalNumber(a*rationalNumber.a, b*rationalNumber.b);
}

RationalNumber RationalNumber::operator/(RationalNumber &rationalNumber) {
    if(rationalNumber.a == 0) {
        std::cout << "ERROR: division by zero in / operation." << std::endl;
        return RationalNumber();
    }
    if(a * rationalNumber.a >= 0)
        return RationalNumber(abs(a*rationalNumber.b), static_cast<unsigned int>(abs(b * rationalNumber.a)));
    return RationalNumber(-abs(a*rationalNumber.b), static_cast<unsigned int>(abs(b * rationalNumber.a)));
}

bool RationalNumber::operator>(RationalNumber &rationalNumber) {
    return a*rationalNumber.b > b*rationalNumber.a;
}

bool RationalNumber::operator<(RationalNumber &rationalNumber) {
    return a*rationalNumber.b < b*rationalNumber.a;
}

bool RationalNumber::operator==(RationalNumber &rationalNumber) {
    return !operator>(rationalNumber) && !operator<(rationalNumber);
}

bool RationalNumber::operator!=(RationalNumber &rationalNumber) {
    return !operator==(rationalNumber);
}

bool RationalNumber::operator>=(RationalNumber &rationalNumber) {
    return !operator<(rationalNumber);
}

bool RationalNumber::operator<=(RationalNumber &rationalNumber) {
    return !operator>(rationalNumber);
}
