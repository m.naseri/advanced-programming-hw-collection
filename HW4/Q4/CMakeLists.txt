cmake_minimum_required(VERSION 3.10)
project(Q4)

set(CMAKE_CXX_STANDARD 17)

add_executable(Q4 main.cpp RationalNumber.cpp RationalNumber.h)