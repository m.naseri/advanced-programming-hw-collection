/** M.Mehdi Naseri 9423115 **/

//
// Created by divar on 4/12/18.
//

#ifndef Q4_RATIONALNUMBER_H
#define Q4_RATIONALNUMBER_H


class RationalNumber {
public:
    RationalNumber() = default;
    RationalNumber(int p, unsigned int q);
    RationalNumber operator+(RationalNumber& rationalNumber);
    RationalNumber operator-(RationalNumber& rationalNumber);
    RationalNumber operator*(RationalNumber& rationalNumber);
    RationalNumber operator/(RationalNumber& rationalNumber);
    bool operator>(RationalNumber& rationalNumber);
    bool operator<(RationalNumber& rationalNumber);
    bool operator==(RationalNumber& rationalNumber);
    bool operator!=(RationalNumber& rationalNumber);
    bool operator>=(RationalNumber& rationalNumber);
    bool operator<=(RationalNumber& rationalNumber);
    void show();

private:
    int a{1};
    unsigned int b{1};
    void simpleForm(int& a, unsigned int& b);
};


#endif //Q4_RATIONALNUMBER_H
