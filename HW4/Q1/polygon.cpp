/** M.Mehdi Naseri 9423115 **/

//
// Created by divar on 4/7/18.
//

#include <iostream>
#include "polygon.h"

polygon::polygon(point *arr, int size) {
    /* Check ArraySize */
    if(size <= 2) {
        std::cout << "ERROR: size should be greater than 2." << std::endl;
        return;
    }

    /* Create Object */
    arraySize = static_cast<unsigned int>(size);
    pointArray = new point[arraySize];
    for(unsigned int i{}; i < arraySize; i++) {
        pointArray[i] = arr[i];
    }
    lineArray = new line[arraySize];
    for(unsigned int i{}; i < arraySize - 1; i++) {
        lineArray[i] = *(pointArray[i].Line(&pointArray[i+1]));
    }
    lineArray[arraySize - 1] = *(pointArray[arraySize - 1].Line(&pointArray[0]));

    /* Avoid 3 points in same direction */
    for(unsigned int i{}; i < arraySize - 1; i++) {
        if(lineArray[i].isParallel(&lineArray[i+1])) {
            std::cout << "ERROR: These points can not create a polygon." << std::endl;
            return;
        }
    }
    if(lineArray[arraySize - 1].isParallel(&lineArray[0])) {
        std::cout << "ERROR: These points can not create a polygon." << std::endl;
        return;
    }
}

polygon::polygon(line *arr, int size) {
    /* Check ArraySize */
    if(size <= 2) {
        std::cout << "ERROR: size should be greater than 2." << std::endl;
        return;
    }

    /* Create Object */
    arraySize = static_cast<unsigned int>(size);
    lineArray = new line[arraySize];
    for(unsigned int i{}; i < arraySize; i++) {
        lineArray[i] = arr[i];
    }
    pointArray = new point[arraySize];
    for(unsigned int i{}; i < arraySize - 1; i++) {
        pointArray[i] = *(lineArray[i].intersection(&lineArray[i+1]));
    }
    pointArray[arraySize - 1] = *(lineArray[arraySize - 1].intersection(&lineArray[0]));

    /* Avoid 3 points in same direction */
    for(unsigned int i{}; i < arraySize - 1; i++) {
        if(lineArray[i].isParallel(&lineArray[i+1])) {
            std::cout << "ERROR: These lines can not create a polygon." << std::endl;
            return;
        }
    }
    if(lineArray[arraySize - 1].isParallel(&lineArray[0])) {
        std::cout << "ERROR: These lines can not create a polygon." << std::endl;
        return;
    }
}

polygon::polygon(polygon &p) {
    arraySize = p.arraySize;
    pointArray = new point[arraySize];
    lineArray = new line[arraySize];
    for(unsigned int i{}; i < arraySize; i++) {
        pointArray[i] = p.pointArray[i];
        lineArray[i] = p.lineArray[i];
    }
}

polygon &polygon::operator=(const polygon &p) {
    if(this == &p)
        return *this;
    delete[] pointArray;
    delete[] lineArray;
    arraySize = p.arraySize;
    pointArray = new point[arraySize];
    lineArray = new line[arraySize];
    for(unsigned int i{}; i < arraySize; i++) {
        pointArray[i] = p.pointArray[i];
        lineArray[i] = p.lineArray[i];
    }
    return *this;
}

bool polygon::isTriangle() {
    return (arraySize == 3);
}

bool polygon::isSquare() {
    if(arraySize == 4)
        if(lineArray[0].isParallel(&lineArray[2]) && lineArray[1].isParallel(&lineArray[3]))
            if(lineArray[0].isPerpendicular(&lineArray[1]))
                if(pointArray[0].distance(&pointArray[1]) == pointArray[1].distance(&pointArray[2]))
                    return true;
    return false;
}

bool polygon::isEqual(polygon p) {
    if(arraySize != p.arraySize)
        return false;

    /* Find index */
    unsigned int index{};
    bool notEqualFlag{true};
    for(unsigned int i{}; i < arraySize; i++) {
        if(pointArray[0].getX() == p.pointArray[i].getX())
            if(pointArray[0].getY() == p.pointArray[i].getY()) {
                notEqualFlag = false;
                index = i;
                break;
            }
    }
    if(notEqualFlag)
        return false;

    /* ClockWise */
    bool equalFlag{true};
    for(unsigned int i{0}; index + i < arraySize; i++) {
        if(pointArray[i].getX() != p.pointArray[index + i].getX() ||
                pointArray[i].getY() != p.pointArray[index + i].getY()) {
            equalFlag = false;
            break;
        }
    }
    if(equalFlag) {
        for(unsigned int i{0}; i < index; i++) {
            if(pointArray[arraySize - index + i].getX() != p.pointArray[i].getX() ||
               pointArray[arraySize - index + i].getY() != p.pointArray[i].getY()) {
                equalFlag = false;
                break;
            }
        }
    }
    if(equalFlag) {
        return true;
    }

    /* AntiClockWise */
    equalFlag = true;
    for (unsigned int i{0}; i <= index; i++) {
        if (pointArray[i].getX() != p.pointArray[index - i].getX() ||
            pointArray[i].getY() != p.pointArray[index - i].getY()) {
            equalFlag = false;
            break;
        }
    }
    if (equalFlag) {
        for (unsigned int i{0}; i + index + 1 < arraySize; i++) {
            if (pointArray[i + index + 1].getX() != p.pointArray[arraySize - 1 - i].getX() ||
                pointArray[i + index + 1].getY() != p.pointArray[arraySize - 1 - i].getY()) {
                equalFlag = false;
                break;
            }
        }
        return equalFlag;
    }
}

void polygon::print() {
    std::cout << "Size: " << arraySize << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "Points: " << std::endl;
    for(unsigned int i{}; i < arraySize; i++) {
        pointArray[i].print();
    }
    std::cout << "---------------" << std::endl;
    std::cout << "Lines: " << std::endl;
    for(unsigned int i{}; i < arraySize; i++) {
        lineArray[i].print();
    }
}

polygon::~polygon() {
    delete[] pointArray;
    delete[] lineArray;
}
