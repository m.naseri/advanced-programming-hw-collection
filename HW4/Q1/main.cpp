/** M.Mehdi Naseri 9423115 **/

#include <iostream>
#include "point.h"
#include "polygon.h"
int main() {
    /* Triangle with Points */
    point p0{0, 0};
    point p1{4, 0};
    point p2{0, 5};
    point trianglePointArray[3]{p0, p1, p2};
    polygon polygon1{trianglePointArray, 3};
//    polygon1.print();

    /* Triangle with Lines */
    line* l0{p0.Line(&p1)};
    line* l1{p1.Line(&p2)};
    line* l2{p2.Line(&p0)};
    line lineArray[3]{*l0, *l1, *l2};
    polygon polygon2{lineArray, 3};
//    polygon2.print();
//    if(polygon1.isTriangle())
//        std::cout << "Triangle" << std::endl;

    /* Square with Points */
    p2.setX(4);
    p2.setY(4);
    point p3{0, 4};
    point squarePointArray[4]{p0, p1, p2, p3};
    polygon polygon3{squarePointArray, 4};
//    polygon3.print();
//    if(polygon3.isSquare())
//        std::cout << "Square" << std::endl;

    /* Triangle with Points */
    point pPrime0{4, 0};
    point pPrime1{0, 0};
    point pPrime2{0, 5};
    point trianglePrimePointArray[3]{pPrime0, pPrime1, pPrime2};
    polygon polygon4{trianglePrimePointArray, 3};
//    if(polygon1.isEqual(polygon4)) {
//        std::cout << "isEqual" << std::endl;
//    }

    return 0;
}
