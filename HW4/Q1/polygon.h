/** M.Mehdi Naseri 9423115 **/

//
// Created by divar on 4/7/18.
//

#ifndef Q1_POLYGON_H
#define Q1_POLYGON_H


#include "point.h"

class polygon {
public:
    polygon(point* arr, int size);
    polygon(line* arr, int size);
    polygon() = default;
    polygon(polygon& p);
    polygon& operator=(const polygon& p);
    bool isTriangle();
    bool isSquare();
    bool isEqual(polygon p);
    void print();
    ~polygon();

private:
    point* pointArray;
    line* lineArray;
    unsigned int arraySize;
};


#endif //Q1_POLYGON_H
